<h1 align="center">Klabin POC K-Rotas</h1>

<p>Esse projeto contém a POC do projeto K-Rotas, que tem como objetivo atualizar as rotas estáticas da Klabin de acordo com o contexto do carregamento e melhorar os rotogramas atuais.</p>

## Pré-requisitos :white_check_mark:

* [Python](https://www.python.org/) - 3.6.9 64-bit ou superior
* [Jupyter](https://jupyter.org/) - 6.0.3 ou superior

## Iniciando :zap:
    git clone git@gitlab.com:techlab1/k-rotas-poc.git
    cd k-rotas-poc
    python3 install.py

## Executando :rocket:
    Executar todas as celulas, em ordem, do Jupyter notebook PreProcessData.ipynb
    (Opcional) executar o notebook RouteVisualization.ipynb
    Executar o notebook RotogramSuggestions.ipynb

<!-- * [create-react-app](https://github.com/facebook/create-react-app) - Componentes -->

## Quem participou? :busts_in_silhouette:

* **Bruno Lippert** - *DB Porto Alegre* - [Email](mailto:bruno.lippert@dbserver.com.br)

