import json
import geopy.distance

class RoadMesh:
    # roadMesh: json decoded road mesh file
    def __init__(self, roadMesh):
        self.distanceLines = roadMesh["distanceLines"]
        self.minPoint = roadMesh["minPoint"]
        self.maxPoint = roadMesh["maxPoint"]
        self.linesLatitude = roadMesh["linesLatitude"]
        self.linesLongitude = roadMesh["linesLongitude"]
        self.matrix = roadMesh["matrix"]
        self.distRealEachLineLongitude = roadMesh["distRealEachLineLongitude"]
        self.distRealEachLineLatitude = roadMesh["distRealEachLineLatitude"]

    # point = Tuple (lat, lng)
    def getInfo(self, point):
        listPoints = self.__getListPoints(point)
        closestPoint = self.__getClosestPoint(point, listPoints)
        return closestPoint

    #The closest point in a list of points of an specific point
    def __getClosestPoint(self, point, listPoints):
        closestPoint = (listPoints[0]["lat"], listPoints[0]["lng"])
        closestPointAllInfo = listPoints[0]
        closestDistance = geopy.distance.geodesic(point, closestPoint).meters

        for poi in listPoints:
            auxPoint = (poi["lat"], poi["lng"])
            auxDistance = geopy.distance.geodesic(point, auxPoint).meters

            if(auxDistance < closestDistance):
                closestPointAllInfo = poi
                closestDistance = auxDistance
                closestPoint = auxPoint
        return closestPointAllInfo
    
    #The list of the nearest points
    def __getListPoints(self, point):
        latIndex = self.__getIndexLatitude(point, self.distRealEachLineLatitude, self.minPoint)
        lngIndex = self.__getIndexLongitude(point, self.distRealEachLineLongitude, self.minPoint)

        if(len(self.matrix[latIndex][lngIndex]) > 0):
            return self.matrix[latIndex][lngIndex]
        else:
            return self.__getClosestListPoints(latIndex, lngIndex)

    #The closeset list where the point is that have any point
    # Ciruculates the area around the original point
    def __getClosestListPoints(self, latIndex, lngIndex):
        actualStep = 1
        
        while(True):
            #first step 1 to left
            lngIndex -= 1
            if self.canGoThere(latIndex, lngIndex):
                if(len(self.matrix[latIndex][lngIndex]) > 0):
                    return self.matrix[latIndex][lngIndex]
            
            #Down
            latIndexAux = latIndex - actualStep
            while(latIndex > latIndexAux):
                latIndex -= 1
                if self.canGoThere(latIndex, lngIndex):
                    if(len(self.matrix[latIndex][lngIndex]) > 0):
                        return self.matrix[latIndex][lngIndex]
            
            #Right
            lngIndexAux = lngIndex + (actualStep * 2)
            while(latIndex < latIndexAux):
                latIndex += 1
                if self.canGoThere(latIndex, lngIndex):
                    if(len(self.matrix[latIndex][lngIndex]) > 0):
                        return self.matrix[latIndex][lngIndex]
            
            #Top
            latIndexAux = latIndex + (actualStep * 2)
            while(latIndex < latIndexAux):
                latIndex += 1
                if self.canGoThere(latIndex, lngIndex):
                    if(len(self.matrix[latIndex][lngIndex]) > 0):
                        return self.matrix[latIndex][lngIndex]
            
            #left
            lngIndexAux = lngIndex - (actualStep * 2)
            while(lngIndex > lngIndexAux):
                lngIndex -= 1
                if self.canGoThere(latIndex, lngIndex):
                    if(len(self.matrix[latIndex][lngIndex]) > 0):
                        return self.matrix[latIndex][lngIndex]
            
            #Down half
            latIndexAux = latIndex - actualStep
            while(latIndex > latIndexAux):
                latIndex -= 1
                if self.canGoThere(latIndex, lngIndex):
                    if(len(self.matrix[latIndex][lngIndex]) > 0):
                        return self.matrix[latIndex][lngIndex]
            actualStep += 1

    def canGoThere(self, latIndex, lngIndex):
        return latIndex >= 0 and lngIndex >= 0 and latIndex < self.linesLatitude and lngIndex < self.linesLongitude

    def __getIndexLatitude(self, point, distLines, minPointLat):
        actualPoint = (point[0], 0)
        minP = (minPointLat["lat"], 0)
        distPointMin = geopy.distance.geodesic(actualPoint, (minP)).meters
        return int(distPointMin / distLines)

    def __getIndexLongitude(self, point, distLines, minPointLng):
        actualPoint = (0, point[1])
        minP = (0, minPointLng["lng"])
        distPointMin = geopy.distance.geodesic(actualPoint, (minP)).meters
        return int(distPointMin / distLines)
