import json
import geopy.distance

class RoadMesh:
    # roadMesh: json decoded road mesh file
    def __init__(self, rotogramMesh):
        self.distanceLines = rotogramMesh["distanceLines"]
        self.minPointLat = rotogramMesh["minPointLat"]
        self.maxPointLat = rotogramMesh["maxPointLat"]
        self.numberOfLines = rotogramMesh["numberOfLines"]
        self.listLines = rotogramMesh["listLines"]
        self.realDistanceBetweenEachLine = rotogramMesh["realDistanceBetweenEachLine"]

    # point = Tuple (lat, lng)
    def isInRoute(self, point, DISTANCE):
        listEdges = self.__getListEdges(point)

        if len(listEdges) == 0:
            return False

        return self.__isNearRoute(point, listEdges, DISTANCE)

    def __isNearRoute(self, point, listEdges, DISTANCE):
        for edge in listEdges:
            pointA = (edge["pointA"]["lat"], edge["pointA"]["lng"])
            pointB = (edge["pointB"]["lat"], edge["pointB"]["lng"])

            lineSegment = LineSegment(pointA, pointB)

            if lineSegment.isPointInLine(self, point, DISTANCE):
                return True

        return False
    
    #The list of the nearest points
    def __getListEdges(self, point):
        lineIndex = self.__getLine(point, self.distRealEachLineLatitude, self.minPointLat)

        if lineIndex >= 0 and lineIndex < len(self.listLines):
            return self.listLines[lineIndex]
        else:
            return []

    def __canGoThere(self, latIndex, lngIndex):
        return latIndex >= 0 and lngIndex >= 0 and latIndex < self.linesLatitude and lngIndex < self.linesLongitude

    def __getLine(self, point, distLines, minPointLat):
        actualPoint = (point[0], 0)
        minP = (minPointLat, 0)

        distPointMin = geopy.distance.geodesic(actualPoint, minP).meters
        return int(distPointMin / distLines)

class LineSegment:
    def __init__(self, pointA: tuple, pointB: tuple):
        self.pointA = pointA
        self.pointB = pointB
        
    def isPointInLine(self, point, distanceInMetersToBeInLine):
        distancePointToSegmentLine = self.__getDistancePointToLineSegment(point)
        
        if self.__isPointCloseToLineSegment(distancePointToSegmentLine, distanceInMetersToBeInLine):
            if self.__isPointOnLineLaterals(point):
                return True
        return False
    
    def __isPointCloseToLineSegment(self, distancePointToSegmentLine, distanceInMetersToBeInLine):
        return distancePointToSegmentLine <= distanceInMetersToBeInLine
    
    def __getDistancePointToLineSegment(self, point):
        closetsPointInLineSegment = self.__closestPointInLine(point)
        return distanceMeteres(point, closetsPointInLineSegment)
    
    def __isPointOnLineLaterals(self, point):
        vectorAP = makeVector(self.pointA, point)
        vectorAB = makeVector(self.pointA, self.pointB)
        
        vectorBP = makeVector(self.pointB, point)
        vectorBA = makeVector(self.pointB, self.pointA)
        
        angleAPAB = angleBetweenVectors(vectorAP, vectorAB)
        angleBPBA = angleBetweenVectors(vectorBP, vectorBA)
        return angleAPAB <= 90 and angleBPBA <= 90

    #https://stackoverflow.com/questions/3120357/get-closest-point-to-a-line
    def __closestPointInLine(self, point):
        a_to_p = [1,1]
        a_to_b = [1,1]

        a_to_p[0] = point[0] - self.pointA[0]
        a_to_p[1] = point[1] - self.pointA[1]
        a_to_b[0] = self.pointB[0] - self.pointA[0]
        a_to_b[1] = self.pointB[1] - self.pointA[1]

        atb2 = a_to_b[0] * a_to_b[0] + a_to_b[1] * a_to_b[1]
        atp_dot_atb = a_to_p[0] * a_to_b[0] + a_to_p[1] * a_to_b[1]
        t = atp_dot_atb / atb2;

        if (t > 1): t = 1
        if (t < 0): t = 0

        return (self.pointA[0] + a_to_b[0] * t, self.pointA[1] + a_to_b[1] * t)