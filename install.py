import os
import json

BLOCKS_LIST_URL = "https://us-central1-k-filas-5f203.cloudfunctions.net/data/blocks"
ORIGINS_LIST_URL = "https://us-central1-k-filas-5f203.cloudfunctions.net/data/origins"
ROTOGRAM_URL = "https://us-central1-k-filas-5f203.cloudfunctions.net/data/rotogramas/ORIGIN/BLOCK"
SUCCESSEFUL_FINISHED_TRIPS_URL = "https://us-central1-k-filas-5f203.cloudfunctions.net/report/successfulFinishedTrips"
BATCH_TRACERS_URL = "https://us-central1-k-filas-5f203.cloudfunctions.net/tracer/getBatchTracers/"

ROTOGRAMS_FILE = "rotograms/"
TRIPS_FILE = "trips/"

BATCH_TRACERS_LENGHT = 10

packagesRequired = ["requests", "geopy", "matplotlib", "numpy", "sklearn", "pandas", "scipy"]

def printStatusMessage(message):
    print("="*100)
    print("-"*5, message, "-"*5)
    print("="*100)

def installRequiredPackages():
    printStatusMessage("Installing required packages")
    for package in packagesRequired:
        os.system('pip3 install '+package)
    

def makeHttpGetResponseToJson(urlGet):
    httpRequestResult = httpClient.get(url = urlGet)
    return httpRequestResult.json()
    
def getListOfBlocks():
    printStatusMessage("Getting blocks list")
    blocksListJson = makeHttpGetResponseToJson(BLOCKS_LIST_URL)
    
    if len(blocksListJson) == 0: raise Exception("Error, no data has been found")
    if len(blocksListJson[0]["blocks"]) == 0: raise Exception("Error, no blocks has been found")

    return blocksListJson[0]["blocks"]

def getListOfOrigins():
    printStatusMessage("Getting origins list")
    originsListJson = makeHttpGetResponseToJson(ORIGINS_LIST_URL)
    
    if len(originsListJson) == 0: raise Exception("Error, no data has been found")
    if len(originsListJson[0]["origins"]) == 0: raise Exception("Error, no origins has been found")

    return originsListJson[0]["origins"]

def getRotogramURL(origin, block):
    return ROTOGRAM_URL.replace("ORIGIN", origin).replace("BLOCK", block)

def getBatchTripsTracersURL(listJson):
    return BATCH_TRACERS_URL+listJson

def writeRotogramToJsonFile(origin, block, rotogramData):
    with open(ROTOGRAMS_FILE+origin+"_"+block+".json", 'w') as rotogramFile:
        json.dump(rotogramData, rotogramFile)

def downloadRotograms(blocks, origins):
    printStatusMessage("Downloading rotograms files")

    finishedDownloadCounter = 0

    for origin in origins:
        for block in blocks:
            currentRotogramURL = getRotogramURL(origin, block)
            rotogramJson = makeHttpGetResponseToJson(currentRotogramURL)
            writeRotogramToJsonFile(origin, block, rotogramJson)

            finishedDownloadCounter += 1
            print("Downloaded",finishedDownloadCounter,"of",len(origins) * len(blocks),"rotograms")

def getSuccessfulFinishedTripsJson():
    printStatusMessage("Getting successeful finhished trips")
    return makeHttpGetResponseToJson(SUCCESSEFUL_FINISHED_TRIPS_URL)

def getTripsTracersJson(listTripsJson):
    return makeHttpGetResponseToJson(getBatchTripsTracersURL(listTripsJson))

def splitListIntoSubListsWithSameLenght(dataList, sublistsSize):
    return [dataList[x:x+sublistsSize] for x in range(0, len(dataList), sublistsSize)]

def getJsonListWithIdFirebaseOnly(listTrips):
    idFirebaseList = []
    for trip in listTrips:
        idFirebaseList.append(trip["idFirebase"])
    return json.dumps(idFirebaseList)

def joinTripDataWithTracers(listTrips, listTracers):
    for trip in listTrips:
        trip["tracers"] = []
        for tracer in listTracers:
            if trip["idFirebase"] == tracer["idFirebase"]:
                trip["tracers"].append(tracer)
    return listTrips

def writeTripsBatch(tripsBatch):
    for trip in tripsBatch:

        truckPrefix = trip["data"]["CAM_PF"]
        origin = trip["data"]["ORIGEM"]
        realOrigin = ""
        try:
            realOrigin = trip["data"]["REAL_ORIGIN"]
        except:
            continue
        if realOrigin == "---":
            continue
        block = trip["data"]["BLOCO_DESTINO"]
        destiny = trip["data"]["DESTINO"]

        with open(TRIPS_FILE+origin+"_"+realOrigin+"_"+block+"_"+destiny+"_"+truckPrefix+".json", 'w') as tripFile:
            json.dump(trip, tripFile, indent=2)

def filterTripsWithoutTwoTracers(tripsList):
    tripsWithTwoTracers = []
    for trip in tripsList:
        if len(trip["tracers"]) == 2:
            tripsWithTwoTracers.append(trip)
    return tripsWithTwoTracers

def makeTripsFiles(successfulFinhedTripsJson):
    printStatusMessage("Downloading tracers batch")
    batchesTripsDataJson = splitListIntoSubListsWithSameLenght(successfulFinhedTripsJson, BATCH_TRACERS_LENGHT)
    
    for batchTrips in batchesTripsDataJson:
        print("Downloading",batchesTripsDataJson.index(batchTrips)+1,"of",len(batchesTripsDataJson))
        tripsJsonFirebaseid = getJsonListWithIdFirebaseOnly(batchTrips)
        tripsBatchUrl = getBatchTripsTracersURL(tripsJsonFirebaseid)
        tripsTracersBatchResult = makeHttpGetResponseToJson(tripsBatchUrl)
        tripsWithTracersJoined = joinTripDataWithTracers(batchTrips, tripsTracersBatchResult)
        tripsFilteredByTwoTracers = filterTripsWithoutTwoTracers(tripsWithTracersJoined)
        writeTripsBatch(tripsFilteredByTwoTracers)


installRequiredPackages()

import requests as httpClient

blocks = getListOfBlocks()
origins = getListOfOrigins()

downloadRotograms(blocks, origins)

successfulFinhedTripsJson = getSuccessfulFinishedTripsJson()

makeTripsFiles(successfulFinhedTripsJson)

printStatusMessage("Installation completed")